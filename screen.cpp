#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include "recognition.cpp"
#include "explotionanimated.h"
#include "random.h"
#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <stdlib.h>     /* srand, rand */
#include <time.h>   
#include <math.h>   


using namespace std;

#define sDown 0
#define sLeft 48
#define sRight 96
#define sUp 144

// int limitPoints=20;
int value=-1;
int centerX=0;
int centerY=0;

double dist(int xMy, int yMy, int xObj,  int yObj)
{
    return sqrt(pow(xMy-xObj,2)+pow(yMy-yObj,2));
}

string intTostring(int num)
{    
    stringstream ss;
    ss << num;
    string str = ss.str();
    return str;
}

sf::String modifyPointsMachine(int points,sf::Font MyFontMachine)
{
    string total="Me:"+intTostring(points);    
    const char* strinTextMachine=total.c_str();
    sf::Unicode::Text ttextMachine(strinTextMachine);
    sf::String TextMachine(ttextMachine, MyFontMachine, 50);
    TextMachine.Move(0.f, 0.f);
    return TextMachine;

}

sf::String modifyPointsUser(int points,sf::Font MyFont)
{
    string total="You:"+intTostring(points);    
    const char* strinTextUser=total.c_str();
    sf::Unicode::Text ttextUser(strinTextUser);
    sf::String TextUser(ttextUser, MyFont, 50);
    TextUser.Move(800.f, 0.f);    
    return TextUser;    
}

sf::String congratulations(sf::Font MyFont,string mOu)
{
    string total;
    if(mOu=="user")
    {
        total="You Win!! ";        
    }
    else
        total="Machine Won!!";    
    // string total="You:"+intTostring(points);    
    const char* strinTextUser=total.c_str();
    sf::Unicode::Text ttextUser(strinTextUser);
    sf::String TextUser(ttextUser, MyFont, 100);
    TextUser.Move(450.f, 450.f);    
    return TextUser;    
}

void makeGame(int & value, int centerX, int centerY)
{
    int machinePoints=0;
    int userPoints=0;
     // Create the main rendering window
    sf::RenderWindow App(sf::VideoMode(1000, 1000, 32), "Yan Kem Poo");
    App.SetPosition(360,10);

    /**** (S) LOADING SCREEN OF GAME *****/
    sf::Image ImaScreenGame;
    sf::Sprite ScreenGame;
    if(ImaScreenGame.LoadFromFile("field.png"))
    {
        cout<<"loaded screen"<<endl;
        ScreenGame.SetImage(ImaScreenGame);
        // App.Draw(ScreenGame);       
        // App.Display(); 
    }
    else
    {
        cout<<"Screen can't be loaded"<<endl;
    }
    /**** (E) LOADING SCREEN OF GAME *****/

    sf::Image tempImage,tempImagePiedra,tempImagePaper,tempImageExplosion;
    bool ifIsLoadedImaExplo=false;
    ExplotionAnimated tempExplotion("explosprite.png",ifIsLoadedImaExplo,4,4);
    // tempExplotion.setNumTinyFrames(4,4);
    cout<<"ifIsLoadedImaExplo: "<<ifIsLoadedImaExplo<<endl;

    sf::Sprite playerSprite,playerSpritePiedra,playerSpritePaper,explosionSprite;

    sf::Shape Polygon;
    Polygon.AddPoint(0, 700,  sf::Color(255, 0, 0),     sf::Color(0, 128, 128));
    Polygon.AddPoint(0, 725,   sf::Color(255, 85, 85),   sf::Color(0, 128, 128));
    Polygon.AddPoint(1000, 700,  sf::Color(255, 170, 170), sf::Color(0, 128, 128));
    Polygon.AddPoint(1000, 725  ,  sf::Color(255, 255, 255), sf::Color(0, 128, 128));
    // Polygon.AddPoint(-50, 50, sf::Color(255, 170, 170), sf::Color(0, 128, 128));
    // Polygon.AddPoint(-50, 0,  sf::Color(255, 85, 85),   sf::Color(0, 128, 128));
    sf::Font MyFont;
    if (!MyFont.LoadFromFile("arial.ttf", 50))
    {
        // Error...
        cout<<"ERROR. no se pudo cargar la fuente"<<endl;
        return;
    }

    if(tempImage.LoadFromFile("Tijeras.png") && tempImagePiedra.LoadFromFile("piedra.png") && 
        tempImagePaper.LoadFromFile("papel.png") && tempImageExplosion.LoadFromFile("explosprite.png") 
        && ifIsLoadedImaExplo )
    {
        playerSprite.SetImage(tempImage);
        playerSpritePiedra.SetImage(tempImagePiedra);
        playerSpritePaper.SetImage(tempImagePaper);   
        explosionSprite.SetImage(tempImageExplosion); 
    }


    /*(S)this need tod be a class after, for the moment this just is like dict for random images*/ 
    string rock="rock";
    int rockNum=1;
    string paper="paper";
    int paperNum=2;
    string scissor="scissor";
    int scissorNum=3;

    vector< pair<string,int> > vectorToRandImages;
    pair<string,int> rockPair(rock,rockNum);
    pair<string,int> paperPair(paper,paperNum);
    pair<string,int> scissorPair(scissor,scissorNum);
    vectorToRandImages.push_back(rockPair);
    vectorToRandImages.push_back(paperPair);
    vectorToRandImages.push_back(scissorPair);
    /*(E)this need tod be a class after, for the moment this just is like dict for random images*/ 



    int velx = 0, vely = 0;
    int x  = 0, y = -100,  moveSpeed= 4;
    int sourceX=0, sourceY=sDown;   
    
   
    // Start game loop
    while (App.IsOpened())
    {
        // Process events
               
        // App.Display(); 

        sf::Event Event;
        while (App.GetEvent(Event))
        {
            // Close window : exit
            if (Event.Type == sf::Event::Closed || Event.Key.Code == sf:: Key:: Escape)
                App.Close();
        }        

        int randNum;
        srand (time(NULL));
        playerSpritePiedra.SetPosition(700,-100); //setting just to go out
        playerSpritePaper.SetPosition(300,-100); //setting just to go out       
        playerSprite.SetPosition(0,-100); //setting just to go out       
        // tempExplotion.SetPosition(0,100);

        int varForOnePointByTime;   
        int varForOnePointByTimeMachine;

        Random RockRand(700);
        RockRand.setRanges(100,900);

        for(;;)
        {

            // sf::String rpta("",MyFont,100);
            // if(userPoints==limitPoints)
            // {
            //     cout<<"kjhsdf"<<endl;
            //     rpta=congratulations(MyFont,"user");
            //     break;
            // }
            // else
            // {
            //     rpta=congratulations(MyFont,"machine");
            //     break;
            // }

            varForOnePointByTime=0;               
            varForOnePointByTimeMachine=0;
            randNum = rand() % 3 + 1;
            // cout<<"randNum: "<<randNum<<endl;
            if(randNum==1)
                x=700;
            if(randNum==2)
                x=300;
            if(randNum==3)
                x=0;

            
            int numToExplotion=0; //this is a temporal num  for explotion ... after will be with threads
            bool ifWasExplodingRock=0;
            bool ifWasExplodingPaper=0;
            bool ifWasExplodingScissor=0;
            cout<<"should be inside by sprite"<<endl;

            while(y<1100)
            {  
                // x = RockRand.setPathRandom(y); // here we can make a rotation of sprite 
                // vely= moveSpeed;            
                //x += velx;
                y += moveSpeed;                
                        
                App.Clear();
                pair<string,int> setUsableImagePair;               
                pair<string,int> whatImageRandomIs=vectorToRandImages[randNum-1];
                if(whatImageRandomIs.second==1)
                {
                    // cout<<"piedra"<<endl;
                    playerSpritePiedra.SetPosition(x,y);                    
                    setUsableImagePair=whatImageRandomIs;
                }                
                
                if(whatImageRandomIs.second==2)
                {                    
                    // cout<<"paper"<<endl;
                    playerSpritePaper.SetPosition(x,y); 
                    setUsableImagePair=whatImageRandomIs;           
                }

                if(whatImageRandomIs.second==3)
                {
                    // cout<<"tijera"<<endl;
                    playerSprite.SetPosition(x,y);
                    setUsableImagePair=whatImageRandomIs;
                }                

                App.Draw(Polygon);
                if(ifWasExplodingScissor==0)
                    App.Draw(playerSprite);
                if(ifWasExplodingRock==0)
                    App.Draw(playerSpritePiedra);
                if(ifWasExplodingPaper==0)
                    App.Draw(playerSpritePaper);                    

                // Display window contents on screen
                                
                // cout<<"VALUE BY REFERENCE: "<<value<<endl;  
                tempExplotion.setSizeAnimation(7);              
                
                if( ( setUsableImagePair.first=="rock") 
                    && (y>0 && y<1050 ))
                {
                    if( (value==2 && varForOnePointByTime==0)  || (numToExplotion>0) ) 
                    {
                        // cout<<"VALUE BY REFERENCE: "<<value<<endl;
                        if(value==2 && varForOnePointByTime==0)
                        {
                            cout<<"You are wining, right with ROCK!!"<<endl;
                            userPoints++;
                            varForOnePointByTime=1;
                            varForOnePointByTimeMachine=1; // to fix the counter of machine, this don't allow that "if" of machine would be exec                        

                            const sf::Vector2f spritePosRock = playerSpritePiedra.GetPosition();
                            int tempRockWidth=spritePosRock.x;
                            int tempRockHeight=spritePosRock.y;
                            // cout<<"x: "<<x<<" y: "<<y<<endl;

                            tempExplotion.setCenterForExplotion(tempRockWidth,tempRockHeight);
                            numToExplotion++;
                        }
                        const sf::Vector2f centerObject = playerSpritePiedra.GetCenter();
                        cout<<"centerObject: "<<centerObject.x<<" "<<centerObject.y<<endl;
                        if (numToExplotion>0 && dist(centerX,centerY,centerObject.x,centerObject.y) < 50 )
                        {
                             // cout<<"insite explotion of rock"<<endl;
                            tempExplotion.makeExplotion(numToExplotion);                            
                            ifWasExplodingRock=1;
                            App.Draw(tempExplotion);  
                        }                                            
                    }                    
                }

                if( ( setUsableImagePair.first=="paper") 
                    && (y>0 && y<1050 ))
                {                 
                    if( (value==3 && varForOnePointByTime==0)  || (numToExplotion>0) ) 
                    {
                        // cout<<"VALUE BY REFERENCE: "<<value<<endl;
                        if(value==3 && varForOnePointByTime==0)
                        {
                            cout<<"You are wining, right with PAPER!!"<<endl;
                            userPoints++;  
                            varForOnePointByTime=1;      
                            varForOnePointByTimeMachine=1; // to fix the counter of machine, this don't allow that "if" of machine would be exec                                     
                            
                            const sf::Vector2f spritePosPaper = playerSpritePaper.GetPosition();
                            int tempPaperWidth = spritePosPaper.x;
                            int tempPaperHeight = spritePosPaper.y;
                            // cout<<"x: "<<x<<" y: "<<y<<endl;

                            tempExplotion.setCenterForExplotion(tempPaperWidth,tempPaperHeight);
                            numToExplotion++;
                        }
                        const sf::Vector2f centerObject = playerSpritePaper.GetCenter();
                        cout<<"centerObject: "<<centerObject.x<<" "<<centerObject.y<<endl;
                        if (numToExplotion>0 && dist(centerX,centerY,centerObject.x,centerObject.y) < 50 )
                        {
                            // cout<<"insite explotion of rock"<<endl;
                            tempExplotion.makeExplotion(numToExplotion);
                            ifWasExplodingPaper=1; // variable to desappear sprite 
                            App.Draw(tempExplotion);  
                        }                                            
                    }                                        
                }

                if( ( setUsableImagePair.first=="scissor") 
                    && (y>0 && y<1050 ))
                {             
                    if( (value==1 && varForOnePointByTime==0)  || (numToExplotion>0) ) 
                    {
                        // cout<<"VALUE BY REFERENCE: "<<value<<endl;       
                        if(value==1 && varForOnePointByTime==0)
                        {
                            cout<<"You are wining, right with SCISSOR!!"<<endl;
                            userPoints++; 
                            varForOnePointByTime=1;      
                            varForOnePointByTimeMachine=1; // to fix the counter of machine, this don't allow that "if" of machine would be exec                 
                            
                            const sf::Vector2f spritePosScissor = playerSprite.GetPosition();
                            int tempScissorWidth = spritePosScissor.x;
                            int tempScissorHeight = spritePosScissor.y;
                            cout<<"x: "<<x<<" y: "<<y<<endl;

                            tempExplotion.setCenterForExplotion(tempScissorWidth,tempScissorHeight);
                            numToExplotion++;
                        }
                        const sf::Vector2f centerObject = playerSprite.GetCenter();
                        cout<<"centerObject: "<<centerObject.x<<" "<<centerObject.y<<endl;
                        if (numToExplotion>0 && dist(centerX,centerY,centerObject.x,centerObject.y) < 50 )
                        {
                            // cout<<"insite explotion of rock"<<endl;
                            tempExplotion.makeExplotion(numToExplotion);
                            ifWasExplodingScissor=1;
                            App.Draw(tempExplotion);  
                        }                                            
                    }   


                }
                if(y>1050  && varForOnePointByTimeMachine==0)
                {
                    cout<<"y: "<<y<<endl;
                    cout<<"Tu Perdiste"<<endl;
                    machinePoints++;
                    varForOnePointByTimeMachine=1;
                }

                 // App.Draw(ScreenGame);       
                App.Draw(modifyPointsUser(userPoints,MyFont));
                App.Draw(modifyPointsMachine(machinePoints,MyFont));
                // App.Draw(rpta);
                App.Display();
                // i++; // for loop of each image !!!
            }

            y=-100;
        }
    }
}


void ThreadFunction(void* UserData)
{    
    // int* value=  static_cast<int*>(UserData);
    // cout<<"value: "<<value<<endl;
    makeGame(value , centerX, centerY);
}

int main()
{
    // int value;    
    sf::Thread Thread(&ThreadFunction);
    Thread.Launch();
    makeRecognition(value,centerX,centerY);
    
    // return EXIT_SUCCESS;
    return 0;
}


//g++ `pkg-config --libs opencv` -lX11 screen.cpp -o main -lsfml-graphics -lsfml-window -lsfml-system 