#include <iostream>
#include "world.h"
#include "recognition.cpp"

int  handX = 0;
int handY = 0 ; 
int value = -1;

void ThreadWork(void* UserData)
{        
	World  first;	
	first.runGame(value,handX,handY);
}

    
int main()
{	
	// cout<<"&handXMain: "<<&handX<<endl;
	// cout<<"&handYMain: "<<&handY<<endl;
	sf::Thread Thread(&ThreadWork);
    Thread.Launch();
    makeRecognition(value,handX,handY);
	return  0;
}