#ifndef RANDOM_H
#define RANDOM_H
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h> 

using namespace std;

class Random
{
	private:
		int rangeXleft;
		int rangeXright;
		int* begin; // y;
		int beginTime; //to set in what 'Y'  this randow make to begin one Sprite
		int rulesSpaceTime;
		int xCurrentPos;
		bool rightOrLeft;
		int buffer;	
		int velocity;
		int modOfRand;

	public:
		Random(int );
		Random( );
		~Random();
		int setPathRandom(int &, int &);
		void setRanges(int,int);
		void placeRand();

};


Random::Random(int currentX)
{		

}

Random::Random()
{		
	// xCurrentPos=currentX;	
	buffer = 0;
	velocity = 4;
	modOfRand = 64 ; // this needs to be multiple of velocity;
}

void Random::setRanges(int xLeft,int xRight)
{
	rangeXleft = xLeft;
	rangeXright = xRight;
}

void Random::placeRand()
{
	srand(time(NULL));
	rightOrLeft = rand() %  2 + 0;
	// return rightOrLeft;
}

int Random::setPathRandom(int & numX,int & numY) // parameter here is y
{		
	if(numY==0)
	{
		placeRand();	
		buffer = numY;
		// helperBuff = numY;
	}
	if(rightOrLeft == 1)
	{		
		numX += velocity;
		
		if(numX >= rangeXright - 200)
		{
			rightOrLeft = 0;
		}
		// cout<<"numY: "<<numY<<endl;
		if( numY % modOfRand == 0)
		{
			// cout<<"inside mod of 48"<<endl;
			placeRand();	
			// cout<<"rightOrLeft: "<<rightOrLeft<<endl;
		}		
	}
	else
	{
		numX -= velocity;
		
		if(numX <= rangeXleft)
		{
			rightOrLeft = 1;
		}
		// cout<<"numY: "<<numY<<" --> "<<numY % modOfRand<<endl;
		if( numY % modOfRand == 0)
		{
			// cout<<"inside mod of 48"<<endl;
			placeRand();	
			// cout<<"rightOrLeft: "<<rightOrLeft<<endl;
		}
	}
	return numX;
}


// vector<int> Random::getPathRandom()
// {
// 	srand (time(NULL));
// 	randNum = rand() % 3 + 1;

// }

Random::~Random()
{	
}

#endif