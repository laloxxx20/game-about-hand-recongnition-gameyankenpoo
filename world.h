#ifndef WORLD_H
#define WORLD_H

#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <unistd.h>
#include <utility> 
#include "rock.h"
// #include "scoreboard.h"

using namespace std;
// using namespace sf;

class World 
{
	 public:
        int* handX;
        int* handY;
        int* value;
	 	World ();	
	 	void  runGame(int & value ,int & handX, int & handY);
        void randomElement();	
        int randomAppear();
        // void ThreadFunction(void* UserData); 	
	 	~World (){};	 		 	

	 
	 private:	 	 
	 	sf::RenderWindow* mainScreen; 
	 	sf::Image ImaScreenGame;        
    	sf::Sprite ScreenGame;
        sf::Sprite Sprite;        
	 	int FinalPoints;
        Scoreboard machineScore;        
        Scoreboard userScore;
        sf::Image imageMachine, imageUser;
        sf::Sprite imaMachine, imaUser;
        pair<int, int> handPoint;
};

World::World()
{
    mainScreen =  new sf::RenderWindow (sf::VideoMode(1000, 800, 32), "Yan Kem Poo");
    mainScreen->SetPosition(360,10);
    
    /**** (S) LOADING SCREEN OF GAME *****/
    if( ImaScreenGame.LoadFromFile("field.png") && imageMachine.LoadFromFile("computer.png") 
       && imageUser.LoadFromFile("user.png") )
    {
        cout<<"loaded screen"<<endl;
        ScreenGame.SetImage(ImaScreenGame);
        ScreenGame.Resize(1000,800);

        imaMachine.SetImage(imageMachine);
        imaMachine.Resize(30,30);
        imaMachine.SetPosition(315,110);
        machineScore.Imagen = imaMachine; 
        
        imaUser.SetImage(imageUser);
        imaUser.Resize(30,30);
        imaUser.SetPosition(660, 110);
        userScore.Imagen = imaUser; 
    }
    else
    {
        cout<<"Screen can't be loaded"<<endl;
    }
    /**** (E) LOADING SCREEN OF GAME *****/
    
    machineScore.LineUpOne = sf::Shape::Line(10, 110, 310, 110, 3, sf::Color(0, 128, 128),0.f,sf::Color(0, 0, 0));
    machineScore.LineDownOne = sf::Shape::Line(10, 140, 310, 140, 3, sf::Color(0, 128, 128),0.f,sf::Color(0, 0, 0));
    machineScore.LineLeftOne = sf::Shape::Line(10, 110, 10, 140, 3, sf::Color(0, 128, 128),0.f,sf::Color(0, 0, 0));
    machineScore.LineRightOne = sf::Shape::Line(310, 110, 310, 140, 3, sf::Color(0, 128, 128),0.f,sf::Color(0, 0, 0));
    userScore.LineUpOne = sf::Shape::Line(690, 110, 990, 110, 3, sf::Color(0, 128, 128),0.f,sf::Color(0, 0, 0));
    userScore.LineDownOne = sf::Shape::Line(690, 140, 990, 140, 3, sf::Color(0, 128, 128),0.f,sf::Color(0, 0, 0));
    userScore.LineLeftOne = sf::Shape::Line(690, 110, 690, 140, 3, sf::Color(0, 128, 128),0.f,sf::Color(0, 0, 0));
    userScore.LineRightOne = sf::Shape::Line(990, 110, 990, 140, 3, sf::Color(0, 128, 128),0.f,sf::Color(0, 0, 0));   


    mainScreen->Draw(ScreenGame);
    mainScreen->Draw(machineScore.LineUpOne);                      
    mainScreen->Draw(machineScore.LineDownOne); 
    mainScreen->Draw(machineScore.LineLeftOne); 
    mainScreen->Draw(machineScore.LineRightOne); 
    mainScreen->Draw(Sprite);    
    // mainScreen->Draw(Line);             
    mainScreen->Display();      
    // while (mainScreen->IsOpened())
    // {}
}

int World::randomAppear()
{
    srand(time(NULL));
    int element = rand() %  1000 + 0;       
    return element;
}

void World::randomElement()
{
    srand(time(NULL));
    int element = rand() %  3 + 1;
    // this->handPoint.first = *handX;
    // cout<<"this->handPoint.first: "<<this->handPoint.first<<endl;
    // this->handPoint.second = *handY;
    // cout<<"this->handPoint.second: "<<this->handPoint.second <<endl;
    cout<<"handXRAM: "<<handX<<endl;
    cout<<"handYRAM: "<<handY<<endl;
    if(element== 1)
    {
        sf:: Sprite rockSprite;
        sf::Image tempImage;
        int randX = 0;
        if(tempImage.LoadFromFile("piedra.png"))
        {
            rockSprite.SetImage(tempImage);
            randX=randomAppear();
            // cout<<"randX: "<<randX<<endl;
            // rockSprite.SetPosition(randX,0);
            // cout<<"rock loaded"<<endl;
        }
        int beginPos=randX;
        Rock see;
        see.setLike("rock");
        see.interfaceData(mainScreen,ScreenGame,rockSprite,beginPos,machineScore,userScore,*handX,*handY,*value);

        // sleep(2);
    }

    if(element == 2)
    {
        sf:: Sprite rockSprite;
        sf::Image tempImage;
        int randX = 0;
        if(tempImage.LoadFromFile("papel.png"))
        {
            rockSprite.SetImage(tempImage);
            randX=randomAppear();
            // cout<<"randX: "<<randX<<endl;
            // rockSprite.SetPosition(randX,0);
            // cout<<"paper loaded"<<endl;
        }
        int beginPos = randX;
        Rock see;
        see.setLike("paper");
        see.interfaceData(mainScreen,ScreenGame,rockSprite,beginPos,machineScore,userScore,*handX,*handY,*value);
        // sleep(2);
    }

    if(element== 3)
    {
        sf:: Sprite rockSprite;
        sf::Image tempImage;
        int randX=0;
        if(tempImage.LoadFromFile("Tijeras.png"))
        {
            rockSprite.SetImage(tempImage);
            randX=randomAppear();
            // cout<<"randX: "<<randX<<endl;
            // rockSprite.SetPosition(randX,0);
            // cout<<"scissor loaded"<<endl;
        }
        int beginPos = randX;
        Rock see;
        see.setLike("scissor");
        see.interfaceData(mainScreen,ScreenGame,rockSprite,beginPos,machineScore,userScore,*handX,*handY,*value);
        // sleep(2);
    }
}

void ThreadFunction(void* UserData)
{
    // Print something...
    World* Object = static_cast<World*>(UserData);
    Object-> randomElement();
}

void  World::runGame(int  & value,int & handX, int & handY)
{    
    
    long begin=0;
    long beginTime=0;
    long timeToAppear=10;
    long sizeScreen=1000;

    while (mainScreen->IsOpened())
    // for(long i=0;i<5;i++)
    {
    	// for(long i=0;i<20;i++)
        for(long i=0;;i++)
    	{                          
            this->handX = &handX;
            cout<<"(this->handX)RAM: "<<(this->handX)<<endl;
            this->handY = &handY;
            cout<<"(this->handY)RAM: "<<(this->handY)<<endl;
            this->value = &value;
            sf::Thread Thread(&ThreadFunction,this);
            Thread.Launch();
            cout<<"i: "<<i<<endl;
    	}
    }
	
}

#endif 