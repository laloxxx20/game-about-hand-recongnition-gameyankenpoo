#ifndef ROCK_H
#define ROCK_H

#include <iostream>
#include <string>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include "random.h"
#include "scoreboard.h"
#include "explotionanimated.h"


using namespace std;
// using namespace sf;

class Rock //: public sf::Thread
{
	 public:
	 	int FinalPoints;
	 	sf::Sprite* playerSprite;
	 	sf::Sprite* screen;
	 	sf::Shape HandCircleShape;
	 	
	 	sf::RenderWindow* currentScreen;
	 	int position;
	 	Random toRock;
	 	Scoreboard* scorePointsM;
	 	Scoreboard* scorePointsU;
	 	int * handPointX;
	 	int * handPointY;
	 	int * value;
	 	ExplotionAnimated* exploElement;
	 	string kind;

	 	Rock()
	 	{
	 		kind = "";
	 	}

	 	double dist(int xMy, int yMy, int xObj,  int yObj)
		{
		    return sqrt(pow(xMy-xObj,2)+pow(yMy-yObj,2));
		}

		void setLike(string k)
		{
			this->kind = k;
		}


	 	void settingData()
	 	{

	 		bool ifIsLoadedImaExplo=false;
			exploElement = new ExplotionAnimated("explosprite.png",ifIsLoadedImaExplo,4,4);
			cout<<"explosprite loaded: "<<ifIsLoadedImaExplo<<endl;
			exploElement->setSizeAnimation(7);

	 		toRock.setRanges(0,1000);
	 		cout<<"settingData"<<endl;

			cout<<"element: "<<kind<<endl;
			int numExplotionIf=0;
			
			for(int i=0 ;i<800;i+=4)
			{			
	 			// this->currentScreen->Display();
	 			int howManyPoints =	10;
	 			int totalScore = 300;
	 			int onePoint= totalScore / howManyPoints;
	 			int zeroPointsUser=0;
	 			int zeroPointsMachine=0;

	 			sf::Shape RectMachine = sf::Shape::Rectangle(10, 110, 12, 140, sf::Color(255,0,0),0.f,sf::Color(0, 0, 0));	 		
				this->currentScreen->Draw(RectMachine);
				sf::Shape RectUser = sf::Shape::Rectangle(690, 110, 692, 140, sf::Color(255,0,0),0.f,sf::Color(0, 0, 0));
				this->currentScreen->Draw(RectUser);
				// if(i==800)
				// {
				// 	zeroPointsMachine += onePoint;	
				// 	sf::Shape RectMachine = sf::Shape::Rectangle(690, 110,onePoint, 140, sf::Color(255,0,0),0.f,sf::Color(0, 0, 0));	 			
				// 	this->currentScreen->Draw(RectMachine);
				// }

				double hx = 1000- ((*handPointX) * 1.5);
				double hy = ((*handPointY) * 1.5); 
				// cout<<"handPoint.first: "<<hx<<" handPoint.second: "<<hy<<endl;
				HandCircleShape = sf::Shape::Circle(hx,hy, 20, sf::Color(0, 128, 128),0.f,sf::Color(0, 0, 0));
				position = toRock.setPathRandom(position,i);
				playerSprite->SetPosition(position,i);
				const sf::Vector2f centerObject = playerSprite->GetPosition();
				// cout<<"centerX: "<<centerObject.x+100<<endl;
				// cout<<"centerY: "<<centerObject.y+75<<endl;
				double distance = dist(hx,hy,centerObject.x+100,centerObject.y+75);
				if(distance < 150 && kind != "" && *this->value!=-1)
				{
					cout<<"is happend a collition"<<endl;
					cout<< "value: "<<*this->value <<endl;
					
					if(kind == "rock" && *this->value == 2) //Rock
					{
						cout<<"Rock is dead"<<endl;
						exploElement->setCenterForExplotion(centerObject.x+50,centerObject.y+50);
						exploElement->makeExplotion(numExplotionIf);  
						this->currentScreen-> Draw(*exploElement); 
						cout<<"HEREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"<<endl;
						cout<<"numExplotionIf: "<<numExplotionIf<<endl;
						if(numExplotionIf == 8)
						{
							zeroPointsUser += onePoint;	
							sf::Shape RectUser = sf::Shape::Rectangle(10, 110,onePoint, 140, sf::Color(255,0,0),0.f,sf::Color(0, 0, 0));	 			
							this->currentScreen->Draw(RectUser);
							return ;
						}
						// this->currentScreen->Display();
					}
					if(kind == "paper" && *this->value == 3) //Paper
					{
						cout<<"Paper is dead"<<endl;	
						exploElement->setCenterForExplotion(centerObject.x+50,centerObject.y+50);
						exploElement->makeExplotion(numExplotionIf); 	
						this->currentScreen-> Draw(*exploElement); 
						if(numExplotionIf == 8)
						{
							zeroPointsUser += onePoint;	
							sf::Shape RectUser = sf::Shape::Rectangle(10, 110,onePoint, 140, sf::Color(255,0,0),0.f,sf::Color(0, 0, 0));
							this->currentScreen->Draw(RectUser);
							return ;
						}
						// this->currentScreen->Display();
					}
					if(kind == "scissor" && *this->value == 1) //Scissor
					{
						cout<<"Scissor is dead"<<endl;						
						exploElement->setCenterForExplotion(centerObject.x+50,centerObject.y+50);
						exploElement->makeExplotion(numExplotionIf); 	
						this->currentScreen-> Draw(*exploElement); 
						if(numExplotionIf == 8)
						{
							zeroPointsUser += onePoint;	
							sf::Shape RectUser = sf::Shape::Rectangle(10, 110,onePoint, 140, sf::Color(255,0,0),0.f,sf::Color(0, 0, 0));	 			
							this->currentScreen->Draw(RectUser);
							return ;
						}
						// this->currentScreen->Display();
					}
				}
				else
				{
					// this->currentScreen->Draw(*playerSprite);
					// this->currentScreen->Display();
					this->currentScreen->Draw(*playerSprite);
					// this->currentScreen->Display();
				}
				
				
				// this->currentScreen->Draw(*playerSprite);
				this->currentScreen->Draw( (*scorePointsM).LineUpOne );
				this->currentScreen->Draw( (*scorePointsM).LineDownOne );
				this->currentScreen->Draw( (*scorePointsM).LineLeftOne );
				this->currentScreen->Draw( (*scorePointsM).LineRightOne );
				this->currentScreen->Draw( (*scorePointsM).Imagen );

				this->currentScreen->Draw( (*scorePointsU).LineUpOne );
				this->currentScreen->Draw( (*scorePointsU).LineDownOne );
				this->currentScreen->Draw( (*scorePointsU).LineLeftOne );
				this->currentScreen->Draw( (*scorePointsU).LineRightOne );
				this->currentScreen->Draw( (*scorePointsU).Imagen );

				this->currentScreen->Draw(HandCircleShape);
				this->currentScreen->Display();

				this->currentScreen->Draw(*screen);
				// this->currentScreen->Draw(*playerSprite);
				// this->currentScreen->Draw( (*scorePointsM).LineUpOne );
				// this->currentScreen->Draw( (*scorePointsM).LineDownOne );
				// this->currentScreen->Draw( (*scorePointsM).LineLeftOne );
				// this->currentScreen->Draw( (*scorePointsM).LineRightOne );
				// this->currentScreen->Draw( (*scorePointsM).Imagen );

				// this->currentScreen->Draw( (*scorePointsU).LineUpOne );
				// this->currentScreen->Draw( (*scorePointsU).LineDownOne );
				// this->currentScreen->Draw( (*scorePointsU).LineLeftOne );
				// this->currentScreen->Draw( (*scorePointsU).LineRightOne );
				// this->currentScreen->Draw( (*scorePointsU).Imagen );

				// this->currentScreen->Draw(HandCircleShape);
				// this->currentScreen->Display();				
			}
	 	};

	 	void interfaceData(sf::RenderWindow* currentScreen,sf::Sprite & screen, sf::Sprite & rock,
	 					   int & pos, Scoreboard & machine, Scoreboard & user , int & handX, int & handY,
	 					   int & valu)
        {
            this->currentScreen=currentScreen;
			this->screen = &screen;
			this->playerSprite = &rock;
			this->position = pos;
			this->scorePointsM = &machine;
			this->scorePointsU = &user;
			// cout<<"handXRAM------>: " <<&handX<<endl;
			// cout<<"handYRAM------>: " <<&handY<<endl;
			this->handPointX = &handX;
			this->handPointY = &handY;
			this-> value = &valu;
            // Launch();
            settingData();
        } 	

       	~Rock()
       	{
       		delete exploElement;
       	} 		 
	 
	 private:	 	 
	 	// virtual void Run()
	 	// {	 		
			// settingData();	 		
	 	// };
	 	
};

#endif 
