#include <SFML/System.hpp>
#include <iostream>
#include <string>

using namespace std;

class MyThread : public sf::Thread
{
    public: 
        int hola;
        void make()
        {
            for (int i = 0; i < 300; ++i)
            {
                hola += i;
                std::cout << "I'm the thread number 2 " << hola <<endl;
                
            }

        };

        void doSomething(int holaa)
        {
            this->hola = holaa;
            Launch();
        }  
    private:

        virtual void Run()
        {
            
            make();
        }
};

int main()
{
    // Create an instance of our custom thread class
    MyThread Thread;

    // Start it !
    Thread.doSomething(100);

    // Print something...
    for (int i = 0; i < 300; ++i)
        std::cout << "I'm the main thread" << std::endl;

    return EXIT_SUCCESS;
}