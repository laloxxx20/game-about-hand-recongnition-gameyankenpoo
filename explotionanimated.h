#ifndef EXPLOTIONANIMATED_H
#define EXPLOTIONANIMATED_H

#include <iostream>
#include "animation.h"
#include <unistd.h>

using namespace std;
// using namespace sf;

class ExplotionAnimated : public Animation 
{
	 public:
	 	ExplotionAnimated(){};
	 	ExplotionAnimated(string name, bool& loaded,int a , int b):Animation(name, loaded,a, b) //here we spawn to explotion
		{			
			widthOriginal = this->GetImage()->GetWidth();
			heightOriginal = this->GetImage()->GetHeight();	
			sizeWidthMiniSprites = widthOriginal/numSpritesRows;
			sizeHeightMiniSprites = heightOriginal/numSpritesColumns;			
		};
	 	~ExplotionAnimated();	 	
	 	void setCenterForExplotion(int, int);
	 	void setTimeExplotion(float);	 	
	 	void makeExplotion(int&);
	 
	 private:	 	 
	 	int xCenter;
	 	int yCenter;	
	 	int widthOriginal;
	 	int heightOriginal;	
	 	int newWidth;		
	 	int newHeight;
	 	int sizeWidthMiniSprites;
	 	int sizeHeightMiniSprites;
};

void ExplotionAnimated::setCenterForExplotion(int x, int y)
{	
	xCenter = x;
	yCenter = y;	
	this->SetImage(this->toAnimate);
	cout<<"xCenter<<yCenter"<<xCenter<<" "<<yCenter<<endl;
	this->SetPosition(xCenter-50,yCenter-50);		
}

void ExplotionAnimated::makeExplotion(int& numPivot)
{
	int initial=1;
	
	if(numPivot < numTinyFrames)
	{
		cout<<"inside makeExplotion"<<endl;
		int whatRowCurrent = numPivot / numSpritesRows;
		int whatColumnCurrent = numPivot % numSpritesRows; 
		// cout<<"whatRowCurrent: "<<whatRowCurrent<<" "<<"whatColumnCurrent: "<<whatColumnCurrent<<endl;
		int currentSecondPointX = (whatColumnCurrent) * sizeWidthMiniSprites;
		int currentsecondPointY = (whatRowCurrent) * sizeHeightMiniSprites;
		int currentFirstPointX =  currentSecondPointX - sizeWidthMiniSprites;
		int currentFirstPointY = currentsecondPointY - sizeHeightMiniSprites;
		// cout<<currentFirstPointX<<" "<<currentFirstPointY<<" "<<currentSecondPointX<<" "<<currentsecondPointY<<endl;
		this->SetSubRect(sf::IntRect(currentFirstPointX,currentFirstPointY,currentSecondPointX,currentsecondPointY));
		this->Resize( (sizeWidthMiniSprites*sizeAnimation) , (sizeHeightMiniSprites*sizeAnimation) );
		numPivot++;
		// sleep(1);
	}	

}

ExplotionAnimated::~ExplotionAnimated()
{

}

#endif 