#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

struct Scoreboard
{     
    sf::Shape LineUpOne;
    sf::Shape LineDownOne;
    sf::Shape LineLeftOne;
    sf::Shape LineRightOne;
    sf::Sprite Imagen;
};