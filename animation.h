#ifndef ANIMATION_H
#define ANIMATION_H
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>

using namespace std;
// using namespace sf;

class Animation : public sf::Sprite 
{
	 public:
	 	Animation();
	 	Animation(string name ,bool& loaded , int Rows, int Columns); //int to know if load of image is correct by reference;
	 	// ~Animation();
	 	void setPathLoadSprite(string );
	 	void setNumTinyFrames(int, int); //setiing num of sprites by rows and columns
	 	void setSizeAnimation(float);

		sf::Image toAnimate;
		float sizeAnimation;
		int numSpritesColumns;
		int numSpritesRows;		
		int numTinyFrames;		
		float velocity;		
	 
	 private:	 	 		
		string path;
};


Animation::Animation()
{
	sizeAnimation=1;
}

Animation::Animation(string name, bool& loaded, int Rows, int Columns)
{
	
	loaded = toAnimate.LoadFromFile(name);
	// cout<<"padre"<<"loaded: "<<loaded<<endl;
	this->SetImage(toAnimate);	
	numSpritesRows=Rows;
	numSpritesColumns=Columns;
	numTinyFrames = numSpritesColumns*numSpritesRows;
}

void Animation::setPathLoadSprite( string name)
{
	path=name;	
	// return toAnimate.LoadFromFile(path);	
}


void Animation::setSizeAnimation(float size)
{
	sizeAnimation = size;
}

void Animation::setNumTinyFrames(int numRows, int numColumns) 
{	
	numSpritesRows = numRows;
	numSpritesColumns = numColumns;
	numTinyFrames = numSpritesColumns*numSpritesRows;
}

#endif 